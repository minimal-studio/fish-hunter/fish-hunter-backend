﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using System;

namespace Minimal.FishHunter.Main
{

    
    class User : System.IDisposable
    {
        public readonly IBinder Binder;
        private readonly IAccountService _Account;
        private readonly IFishFarmService _FishFarm;
        readonly Regulus.Utility.StageMachine _Machine;
        public User(IBinder binder, Common.IAccountService account,IFishFarmService fish_farm)
        {
            this.Binder = binder;
            this._Account = account;
            _FishFarm = fish_farm;
            _Machine = new Regulus.Utility.StageMachine();

            _ToVerify();

        }

        private void _ToVerify()
        {
            var stage = new UserVerify(Binder,_Account);
            stage.DoneEvent += _ToFishFarm;
        }
        void _ToFishFarm(IAccount account)
        {
            var stage = new UserFishFarm(Binder,account, _FishFarm);
            stage.DoneEvent += _ToVerify;
        }
        public void Dispose()
        {
            _Machine.Clean();
        }
    }
}
