﻿using Minimal.FishHunter.Common;
using System;
using System.Linq;
namespace Minimal.FishHunter.Main
{
    class FishFarmPool
    {
        readonly System.Collections.Generic.List<FishFarm> _Farms;
        public FishFarmPool()
        {
            _Farms = new System.Collections.Generic.List<FishFarm>();
        }
        internal IFishFarm Join(Guid id)
        {
            var farms = from f in _Farms where f.NotFull() select f;
            var farm = farms.FirstOrDefault();
            return null;
        }
    }
}