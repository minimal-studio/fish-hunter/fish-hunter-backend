﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using System;

namespace Minimal.FishHunter.Main
{
    class FishFarm : IFishFarm
    {
        public FishFarm()
        { 

        }
        Property<long> IFishFarm.Ticks => throw new NotImplementedException();

        INotifier<IFishFarmPlayer> IFishFarm.Players => throw new NotImplementedException();

        INotifier<IFishFarmFish> IFishFarm.Fishs => throw new NotImplementedException();

        internal bool NotFull()
        {
            throw new NotImplementedException();
        }

        void IFishFarm.ChangeWeapon(Guid id, WEAPON weapon)
        {
            throw new NotImplementedException();
        }

        void IFishFarm.Exit(Guid id)
        {
            throw new NotImplementedException();
        }

        void IFishFarm.Fire(Guid id)
        {
            throw new NotImplementedException();
        }

        void IFishFarm.Hit(Guid id, int fish)
        {
            throw new NotImplementedException();
        }

        void IFishFarm.Rotation(Guid id, ROTATION rotation)
        {
            throw new NotImplementedException();
        }


    }
}