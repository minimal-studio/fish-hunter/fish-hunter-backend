﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using System;

namespace Minimal.FishHunter.Main
{
    internal class AccountService : Common.IAccountService
    {
        readonly System.Collections.Generic.Dictionary<string, Account> _Accounts;

        public AccountService()
        {
            _Accounts = new System.Collections.Generic.Dictionary<string, Account>();
        }
        Value<IAccount> IAccountService.FindAccount(string account_name)
        {
            Account account;
            if (_Accounts.TryGetValue(account_name, out account))
            {
                return account;
            }

            account = new Account(account_name);
            _Accounts.Add(account_name,account);

            return account;
        }

        internal void Dispose()
        {
            
        }
    }
}