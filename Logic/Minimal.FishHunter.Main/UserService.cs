﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using System;
using System.Linq;
namespace Minimal.FishHunter.Main
{
    public class UserService : System.IDisposable
    {
        readonly System.Collections.Generic.List<User> _Users;
        private readonly IAccountService _Account;
        private readonly IFishFarmService _FishFarm;

        public UserService(Common.IAccountService account, Common.IFishFarmService fish_farm)
        {
            _Users = new System.Collections.Generic.List<User>();
            this._Account = account;
            _FishFarm = fish_farm;
        }
        internal void Add(IBinder binder)
        {
            _Users.Add(new User(binder,_Account, _FishFarm));
        }

        internal void Remove(IBinder binder)
        {
            var user = _Users.Single(u=>u.Binder == binder);
            _Users.Remove(user);
            user.Dispose(); 
        }

        void IDisposable.Dispose()
        {
            Dispose();
        }

        internal void Dispose()
        {
            foreach (var user in _Users)
            {
                user.Dispose();
            }
        }
    }
}
