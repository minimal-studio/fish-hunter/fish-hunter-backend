﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using Regulus.Utility;
using System;

namespace Minimal.FishHunter.Main
{
    internal class UserFishFarm :  Regulus.Utility.IBootable 
    {
        private readonly IBinder _Binder;
        private readonly IAccount _Account;
        private readonly IFishFarmService _Service;
        readonly Regulus.Utility.StageMachine _Machine;

        public event System.Action DoneEvent;
        public UserFishFarm(IBinder binder, IAccount account,IFishFarmService service)
        {
            this._Binder = binder;
            this._Account = account;
            this._Service = service;
            _Machine = new StageMachine();
        }

        

        void IBootable.Launch()
        {
            _ToQuery();
        }

        private void _ToQuery()
        {
            var stage = new UserFishFarmQuery(_Service, _Account);
            stage.DoneEvent += _ToPlay;
            _Machine.Push(stage);
        }

        private void _ToPlay(IFishFarm farm)
        {
            var stage = new UserFishFarmPlay(_Binder,farm, _Account);
            stage.DoneEvent += DoneEvent;
            _Machine.Push(stage);
        }

        void IBootable.Shutdown()
        {
            _Machine.Clean();
        }

    }
}