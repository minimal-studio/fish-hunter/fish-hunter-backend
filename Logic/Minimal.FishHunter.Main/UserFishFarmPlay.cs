﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using Regulus.Utility;
using System;

namespace Minimal.FishHunter.Main
{
    internal class UserFishFarmPlay : Regulus.Utility.IBootable , IUserFishFarmPlayer , IUserFishFarm
    {
        private readonly IBinder _Binder;
        private IFishFarm _Farm;
        private IAccount _Account;
        public event System.Action DoneEvent;
        public UserFishFarmPlay(IBinder binder , IFishFarm farm, IAccount account)
        {
            this._Binder = binder;
            this._Farm = farm;
            _Account = account;
        }

        Property<Guid> IUserFishFarmPlayer.Id => _Account.Id;

        Property<long> IUserFishFarm.Ticks => _Farm.Ticks;

        void IUserFishFarmPlayer.Exit()
        {
            DoneEvent();
           
        }

        void IUserFishFarmPlayer.Fire()
        {
            _Farm.Fire(_Account.Id);
        }

        void IBootable.Launch()
        {
            _Farm.Players.Unsupply += _Unsupply;
            
            _Farm.Players.Supply += _Supply;

            _Farm.Fishs.Unsupply += _Unsupply;
            _Farm.Fishs.Supply += _Supply;
        }

        

        void IUserFishFarmPlayer.Rotation(ROTATION rot)
        {
            _Farm.Rotation(_Account.Id , rot);
        }

        void IBootable.Shutdown()
        {
            _Farm.Players.Unsupply -= _Unsupply;
            
            _Farm.Players.Supply -= _Supply;

            _Farm.Fishs.Unsupply -= _Unsupply;
            _Farm.Fishs.Supply -= _Supply;
            _Farm.Exit(_Account.Id);
        }

      

        private void _Supply(IFishFarmPlayer obj)
        {
            _Binder.Bind<IFishFarmPlayer>(obj);
        }

        private void _Unsupply(IFishFarmPlayer obj)
        {
            _Binder.Unbind<IFishFarmPlayer>(obj);
        }
        private void _Supply(IFishFarmFish obj)
        {
            _Binder.Bind<IFishFarmFish>(obj);
        }

        private void _Unsupply(IFishFarmFish obj)
        {
            _Binder.Unbind<IFishFarmFish>(obj);
        }

        void IUserFishFarmPlayer.Hit(int fish)
        {
            _Farm.Hit(_Account.Id, fish);
        }

        void IUserFishFarmPlayer.ChangeWeapon(WEAPON weapon)
        {
            _Farm.ChangeWeapon(_Account.Id , weapon);
        }
    }
}