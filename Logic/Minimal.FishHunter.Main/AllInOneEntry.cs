﻿using Regulus.Remote;
using System;
using System.Threading;

namespace Minimal.FishHunter.Main
{

    public class AllInOneEntry : Regulus.Remote.IEntry , System.IDisposable
    {
        readonly AccountService _AccountService;
        readonly FishFarmService _FishFarmService;
        readonly UserService _UserService;
        
        readonly System.Collections.Concurrent.ConcurrentQueue<IBinder> _AddUsers;
        readonly System.Collections.Concurrent.ConcurrentQueue<IBinder> _RemoveUsers;
        readonly Regulus.Remote.ThreadUpdater _Updater;
        public AllInOneEntry()
        {
            _FishFarmService = new FishFarmService();
            _AccountService = new AccountService();
            _UserService = new UserService(_AccountService, _FishFarmService);
            _RemoveUsers = new System.Collections.Concurrent.ConcurrentQueue<IBinder>();
            _AddUsers = new System.Collections.Concurrent.ConcurrentQueue<IBinder>();

            _Updater = new ThreadUpdater(_Run);
            _Updater.Start();
        }

        void _Run()
        {
            IBinder binder;
            while (_AddUsers.TryDequeue(out binder))
            {
                _UserService.Add(binder);
            }

            while (_RemoveUsers.TryDequeue(out binder))
            {
                _UserService.Remove(binder);
            }

            
        }
        void IBinderProvider.AssignBinder(IBinder binder, object state)
        {
            binder.BreakEvent += () => { _RemoveUsers.Enqueue(binder); };
            _RemoveUsers.Enqueue(binder);
        }

        void IDisposable.Dispose()
        {
            _Updater.Stop();
            _UserService.Dispose();
            _AccountService.Dispose();
            _FishFarmService.Dispose();
        }
    }
}
