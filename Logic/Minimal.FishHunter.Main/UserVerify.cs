﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using Regulus.Utility;

namespace Minimal.FishHunter.Main
{
    internal class UserVerify : IUserVerify , Regulus.Utility.IBootable
    {
        private readonly IBinder _Binder;
        private IAccountService _Account;
        public event System.Action<IAccount> DoneEvent;
        public UserVerify(IBinder binder, IAccountService account)
        {
            this._Binder = binder;
            _Account = account;
        }

        Value<bool> IUserVerify.Verify(string account, string password)
        {
            var ret = new Value<bool>();
            var findResult = _Account.FindAccount(account);

            findResult.OnValue += (acc) => {
                var result = acc.Password.Value == password;
                ret.SetValue(result);
                if (result)
                    DoneEvent(acc);
            };

            return ret;
        }

        void IBootable.Launch()
        {
            _Binder.Bind<IUserVerify>(this);
        }

        void IBootable.Shutdown()
        {
            _Binder.Unbind<IUserVerify>(this);
        }
    }
}