﻿using Minimal.FishHunter.Common;
using Regulus.Utility;
using System;

namespace Minimal.FishHunter.Main
{
    internal class UserFishFarmQuery : Regulus.Utility.IBootable
    {
        private IFishFarmService _Service;
        private IAccount _Account;
        public event System.Action<IFishFarm> DoneEvent;
        public UserFishFarmQuery(IFishFarmService service, IAccount account)
        {
            _Service = service;
            _Account = account;
        }

        void IBootable.Launch()
        {
            _Service.Query(_Account.Id.Value).OnValue += _QueryDone;
        }

        private void _QueryDone(IFishFarm farm)
        {
            DoneEvent(farm);
        }

        void IBootable.Shutdown()
        {
        }
    }
}