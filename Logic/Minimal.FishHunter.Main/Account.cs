﻿using Minimal.FishHunter.Common;
using Regulus.Remote;
using System;

namespace Minimal.FishHunter.Main
{
    class Account : Common.IAccount
    {
        private readonly string _Name;

        public Account(string account_name)
        {
            this._Name = account_name;
        }

        Property<Guid> IAccount.Id => new Property<Guid>(Guid.NewGuid());

        Property<string> IAccount.Password => new Property<string>("");
    }
}