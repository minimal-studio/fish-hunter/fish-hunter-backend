using System;
using Xunit;

namespace Minimal.FishHunter.Tests
{
    public class FishFarmPoolTests
    {

        public FishFarmPoolTests()
        {

        }

        // 多出來的人數會開另外一間房。
        [Fact]
        public void JoinTest()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            var id4 = Guid.NewGuid();
            var id5 = Guid.NewGuid();

            var pool = new Main.FishFarmPool();

            Common.IFishFarm farm1 = pool.Join(id1);
            Common.IFishFarm farm2 = pool.Join(id2);
            Common.IFishFarm farm3 = pool.Join(id3);
            Common.IFishFarm farm4 = pool.Join(id4);
            Common.IFishFarm farm5 = pool.Join(id5);

            Xunit.Assert.True(farm1 == farm2);
            Xunit.Assert.True(farm1 == farm3);
            Xunit.Assert.True(farm1 == farm4);
            Xunit.Assert.True(farm1 != farm5);


        }
    }
}
