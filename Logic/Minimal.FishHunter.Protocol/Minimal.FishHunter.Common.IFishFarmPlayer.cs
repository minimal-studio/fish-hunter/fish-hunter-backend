   
    using System;  
    
    using System.Collections.Generic;
    
    namespace Minimal.FishHunter.Common.Ghost 
    { 
        public class CIFishFarmPlayer : Minimal.FishHunter.Common.IFishFarmPlayer , Regulus.Remote.IGhost
        {
            readonly bool _HaveReturn ;
            
            readonly long _GhostIdName;
            
            
            
            public CIFishFarmPlayer(long id, bool have_return )
            {                
                //Id
//Location
//Rotation
//Weapon
                _HaveReturn = have_return ;
                _GhostIdName = id; 
                
            }
            

            long Regulus.Remote.IGhost.GetID()
            {
                return _GhostIdName;
            }

            bool Regulus.Remote.IGhost.IsReturnType()
            {
                return _HaveReturn;
            }
            object Regulus.Remote.IGhost.GetInstance()
            {
                return this;
            }

            private event Regulus.Remote.CallMethodCallback _CallMethodEvent;

            event Regulus.Remote.CallMethodCallback Regulus.Remote.IGhost.CallMethodEvent
            {
                add { this._CallMethodEvent += value; }
                remove { this._CallMethodEvent -= value; }
            }

            private event Regulus.Remote.EventNotifyCallback _AddEventEvent;

            event Regulus.Remote.EventNotifyCallback Regulus.Remote.IGhost.AddEventEvent
            {
                add { this._AddEventEvent += value; }
                remove { this._AddEventEvent -= value; }
            }

            private event Regulus.Remote.EventNotifyCallback _RemoveEventEvent;

            event Regulus.Remote.EventNotifyCallback Regulus.Remote.IGhost.RemoveEventEvent
            {
                add { this._RemoveEventEvent += value; }
                remove { this._RemoveEventEvent -= value; }
            }
            event Regulus.Remote.PropertyNotifierCallback _AddSupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.AddSupplyNoitfierEvent
            {

                add
                {
                    _AddSupplyNoitfierEvent += value;
                }

                remove
                {
                    _AddSupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _RemoveSupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.RemoveSupplyNoitfierEvent
            {
                add
                {
                    _RemoveSupplyNoitfierEvent += value;
                }

                remove
                {
                    _RemoveSupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _AddUnsupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.AddUnsupplyNoitfierEvent
            {
                add
                {
                    _AddUnsupplyNoitfierEvent += value;
                }

                remove
                {
                    _AddUnsupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _RemoveUnsupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.RemoveUnsupplyNoitfierEvent
            {
                add
                {
                    _RemoveUnsupplyNoitfierEvent += value;
                }

                remove
                {
                    _RemoveUnsupplyNoitfierEvent -= value;
                }
            }
            

                public Regulus.Remote.Property<System.Guid> _Id= new Regulus.Remote.Property<System.Guid>();
                Regulus.Remote.Property<System.Guid> Minimal.FishHunter.Common.IFishFarmPlayer.Id { get{ return _Id;} }

                public Regulus.Remote.Property<System.Int32> _Location= new Regulus.Remote.Property<System.Int32>();
                Regulus.Remote.Property<System.Int32> Minimal.FishHunter.Common.IFishFarmPlayer.Location { get{ return _Location;} }

                public Regulus.Remote.Property<System.Single> _Rotation= new Regulus.Remote.Property<System.Single>();
                Regulus.Remote.Property<System.Single> Minimal.FishHunter.Common.IFishFarmPlayer.Rotation { get{ return _Rotation;} }

                public Regulus.Remote.Property<Minimal.FishHunter.Common.WEAPON> _Weapon= new Regulus.Remote.Property<Minimal.FishHunter.Common.WEAPON>();
                Regulus.Remote.Property<Minimal.FishHunter.Common.WEAPON> Minimal.FishHunter.Common.IFishFarmPlayer.Weapon { get{ return _Weapon;} }

                public Regulus.Remote.GhostEventHandler _FireEvent = new Regulus.Remote.GhostEventHandler();
                event System.Action<Minimal.FishHunter.Common.Bullet> Minimal.FishHunter.Common.IFishFarmPlayer.FireEvent
                {
                    add { 
                            var id = _FireEvent.Add(value);
                            _AddEventEvent(typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetEvent("FireEvent"),id);
                        }
                    remove { 
                                var id = _FireEvent.Remove(value);
                                _RemoveEventEvent(typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetEvent("FireEvent"),id);
                            }
                }
                
            
        }
    }
