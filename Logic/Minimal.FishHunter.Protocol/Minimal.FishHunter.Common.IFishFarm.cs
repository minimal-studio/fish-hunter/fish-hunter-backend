   
    using System;  
    
    using System.Collections.Generic;
    
    namespace Minimal.FishHunter.Common.Ghost 
    { 
        public class CIFishFarm : Minimal.FishHunter.Common.IFishFarm , Regulus.Remote.IGhost
        {
            readonly bool _HaveReturn ;
            
            readonly long _GhostIdName;
            
            
            
            public CIFishFarm(long id, bool have_return )
            {                
                //Ticks
//Players
_Players = new Regulus.Remote.GhostNotifier<Minimal.FishHunter.Common.IFishFarmPlayer>((p) => _AddSupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Players"), p), (p) => _RemoveSupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Players"),p), (p) => _AddUnsupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Players"), p), (p) => _RemoveUnsupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Players"),p));
//Fishs
_Fishs = new Regulus.Remote.GhostNotifier<Minimal.FishHunter.Common.IFishFarmFish>((p) => _AddSupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Fishs"), p), (p) => _RemoveSupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Fishs"),p), (p) => _AddUnsupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Fishs"), p), (p) => _RemoveUnsupplyNoitfierEvent(typeof(IFishFarm).GetProperty("Fishs"),p));
                _HaveReturn = have_return ;
                _GhostIdName = id; 
                
            }
            

            long Regulus.Remote.IGhost.GetID()
            {
                return _GhostIdName;
            }

            bool Regulus.Remote.IGhost.IsReturnType()
            {
                return _HaveReturn;
            }
            object Regulus.Remote.IGhost.GetInstance()
            {
                return this;
            }

            private event Regulus.Remote.CallMethodCallback _CallMethodEvent;

            event Regulus.Remote.CallMethodCallback Regulus.Remote.IGhost.CallMethodEvent
            {
                add { this._CallMethodEvent += value; }
                remove { this._CallMethodEvent -= value; }
            }

            private event Regulus.Remote.EventNotifyCallback _AddEventEvent;

            event Regulus.Remote.EventNotifyCallback Regulus.Remote.IGhost.AddEventEvent
            {
                add { this._AddEventEvent += value; }
                remove { this._AddEventEvent -= value; }
            }

            private event Regulus.Remote.EventNotifyCallback _RemoveEventEvent;

            event Regulus.Remote.EventNotifyCallback Regulus.Remote.IGhost.RemoveEventEvent
            {
                add { this._RemoveEventEvent += value; }
                remove { this._RemoveEventEvent -= value; }
            }
            event Regulus.Remote.PropertyNotifierCallback _AddSupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.AddSupplyNoitfierEvent
            {

                add
                {
                    _AddSupplyNoitfierEvent += value;
                }

                remove
                {
                    _AddSupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _RemoveSupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.RemoveSupplyNoitfierEvent
            {
                add
                {
                    _RemoveSupplyNoitfierEvent += value;
                }

                remove
                {
                    _RemoveSupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _AddUnsupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.AddUnsupplyNoitfierEvent
            {
                add
                {
                    _AddUnsupplyNoitfierEvent += value;
                }

                remove
                {
                    _AddUnsupplyNoitfierEvent -= value;
                }
            }

            event Regulus.Remote.PropertyNotifierCallback _RemoveUnsupplyNoitfierEvent;
            event Regulus.Remote.PropertyNotifierCallback Regulus.Remote.IGhost.RemoveUnsupplyNoitfierEvent
            {
                add
                {
                    _RemoveUnsupplyNoitfierEvent += value;
                }

                remove
                {
                    _RemoveUnsupplyNoitfierEvent -= value;
                }
            }
            
                void Minimal.FishHunter.Common.IFishFarm.Fire(System.Guid _1)
                {                    

                    Regulus.Remote.IValue returnValue = null;
                    var info = typeof(Minimal.FishHunter.Common.IFishFarm).GetMethod("Fire");
                    _CallMethodEvent(info , new object[] {_1} , returnValue);                    
                    
                }

                
 

                void Minimal.FishHunter.Common.IFishFarm.Rotation(System.Guid _1,Minimal.FishHunter.Common.ROTATION _2)
                {                    

                    Regulus.Remote.IValue returnValue = null;
                    var info = typeof(Minimal.FishHunter.Common.IFishFarm).GetMethod("Rotation");
                    _CallMethodEvent(info , new object[] {_1,_2} , returnValue);                    
                    
                }

                
 

                void Minimal.FishHunter.Common.IFishFarm.Exit(System.Guid _1)
                {                    

                    Regulus.Remote.IValue returnValue = null;
                    var info = typeof(Minimal.FishHunter.Common.IFishFarm).GetMethod("Exit");
                    _CallMethodEvent(info , new object[] {_1} , returnValue);                    
                    
                }

                
 

                void Minimal.FishHunter.Common.IFishFarm.Hit(System.Guid _1,System.Int32 _2)
                {                    

                    Regulus.Remote.IValue returnValue = null;
                    var info = typeof(Minimal.FishHunter.Common.IFishFarm).GetMethod("Hit");
                    _CallMethodEvent(info , new object[] {_1,_2} , returnValue);                    
                    
                }

                
 

                void Minimal.FishHunter.Common.IFishFarm.ChangeWeapon(System.Guid _1,Minimal.FishHunter.Common.WEAPON _2)
                {                    

                    Regulus.Remote.IValue returnValue = null;
                    var info = typeof(Minimal.FishHunter.Common.IFishFarm).GetMethod("ChangeWeapon");
                    _CallMethodEvent(info , new object[] {_1,_2} , returnValue);                    
                    
                }

                


                public Regulus.Remote.Property<System.Int64> _Ticks= new Regulus.Remote.Property<System.Int64>();
                Regulus.Remote.Property<System.Int64> Minimal.FishHunter.Common.IFishFarm.Ticks { get{ return _Ticks;} }

            readonly Regulus.Remote.GhostNotifier<Minimal.FishHunter.Common.IFishFarmPlayer> _Players;
            Regulus.Remote.INotifier<Minimal.FishHunter.Common.IFishFarmPlayer> Minimal.FishHunter.Common.IFishFarm.Players { get{ return _Players;} }

            readonly Regulus.Remote.GhostNotifier<Minimal.FishHunter.Common.IFishFarmFish> _Fishs;
            Regulus.Remote.INotifier<Minimal.FishHunter.Common.IFishFarmFish> Minimal.FishHunter.Common.IFishFarm.Fishs { get{ return _Fishs;} }

            
        }
    }
