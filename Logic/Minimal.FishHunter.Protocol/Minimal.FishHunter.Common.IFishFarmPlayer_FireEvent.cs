
    using System;  
    using System.Collections.Generic;
    
    namespace Minimal.FishHunter.Common.Invoker.IFishFarmPlayer 
    { 
        public class FireEvent : Regulus.Remote.IEventProxyCreator
        {

            Type _Type;
            string _Name;
            
            public FireEvent()
            {
                _Name = "FireEvent";
                _Type = typeof(Minimal.FishHunter.Common.IFishFarmPlayer);                   
            
            }
            Delegate Regulus.Remote.IEventProxyCreator.Create(long soul_id,int event_id,long handler_id, Regulus.Remote.InvokeEventCallabck invoke_Event)
            {                
                var closure = new Regulus.Remote.GenericEventClosure<Minimal.FishHunter.Common.Bullet>(soul_id , event_id ,handler_id, invoke_Event);                
                return new Action<Minimal.FishHunter.Common.Bullet>(closure.Run);
            }
        

            Type Regulus.Remote.IEventProxyCreator.GetType()
            {
                return _Type;
            }            

            string Regulus.Remote.IEventProxyCreator.GetName()
            {
                return _Name;
            }            
        }
    }
                