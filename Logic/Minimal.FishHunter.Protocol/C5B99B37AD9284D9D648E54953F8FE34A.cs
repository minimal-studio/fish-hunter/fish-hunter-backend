
            using System;  
            using System.Collections.Generic;
            
            
                public class C5B99B37AD9284D9D648E54953F8FE34A : Regulus.Remote.IProtocol
                {
                    readonly Regulus.Remote.InterfaceProvider _InterfaceProvider;
                    readonly Regulus.Remote.EventProvider _EventProvider;
                    readonly Regulus.Remote.MemberMap _MemberMap;
                    readonly Regulus.Serialization.ISerializer _Serializer;
                    readonly System.Reflection.Assembly _Base;
                    public C5B99B37AD9284D9D648E54953F8FE34A()
                    {
                        _Base = System.Reflection.Assembly.Load("Minimal.FishHunter.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                        var types = new Dictionary<Type, Type>();
                        types.Add(typeof(Minimal.FishHunter.Common.IAccount) , typeof(Minimal.FishHunter.Common.Ghost.CIAccount) );
types.Add(typeof(Minimal.FishHunter.Common.IAccountService) , typeof(Minimal.FishHunter.Common.Ghost.CIAccountService) );
types.Add(typeof(Minimal.FishHunter.Common.IFishFarm) , typeof(Minimal.FishHunter.Common.Ghost.CIFishFarm) );
types.Add(typeof(Minimal.FishHunter.Common.IFishFarmFish) , typeof(Minimal.FishHunter.Common.Ghost.CIFishFarmFish) );
types.Add(typeof(Minimal.FishHunter.Common.IFishFarmPlayer) , typeof(Minimal.FishHunter.Common.Ghost.CIFishFarmPlayer) );
types.Add(typeof(Minimal.FishHunter.Common.IFishFarmService) , typeof(Minimal.FishHunter.Common.Ghost.CIFishFarmService) );
types.Add(typeof(Minimal.FishHunter.Common.IUserFishFarm) , typeof(Minimal.FishHunter.Common.Ghost.CIUserFishFarm) );
types.Add(typeof(Minimal.FishHunter.Common.IUserFishFarmPlayer) , typeof(Minimal.FishHunter.Common.Ghost.CIUserFishFarmPlayer) );
types.Add(typeof(Minimal.FishHunter.Common.IUserVerify) , typeof(Minimal.FishHunter.Common.Ghost.CIUserVerify) );
                        _InterfaceProvider = new Regulus.Remote.InterfaceProvider(types);
                        var eventClosures = new List<Regulus.Remote.IEventProxyCreator>();
                        eventClosures.Add(new Minimal.FishHunter.Common.Invoker.IFishFarmPlayer.FireEvent() );
                        _EventProvider = new Regulus.Remote.EventProvider(eventClosures);
                        _Serializer = new Regulus.Serialization.Serializer(new Regulus.Serialization.DescriberBuilder(typeof(Minimal.FishHunter.Common.Bullet),typeof(Minimal.FishHunter.Common.Movement),typeof(Minimal.FishHunter.Common.Position),typeof(Minimal.FishHunter.Common.ROTATION),typeof(Minimal.FishHunter.Common.WEAPON),typeof(Regulus.Remote.ClientToServerOpCode),typeof(Regulus.Remote.PackageAddEvent),typeof(Regulus.Remote.PackageCallMethod),typeof(Regulus.Remote.PackageErrorMethod),typeof(Regulus.Remote.PackageInvokeEvent),typeof(Regulus.Remote.PackageLoadSoul),typeof(Regulus.Remote.PackageLoadSoulCompile),typeof(Regulus.Remote.PackageNotifier),typeof(Regulus.Remote.PackageNotifierEventHook),typeof(Regulus.Remote.PackageProtocolSubmit),typeof(Regulus.Remote.PackageRelease),typeof(Regulus.Remote.PackageRemoveEvent),typeof(Regulus.Remote.PackageReturnValue),typeof(Regulus.Remote.PackageSetProperty),typeof(Regulus.Remote.PackageSetPropertyDone),typeof(Regulus.Remote.PackageUnloadSoul),typeof(Regulus.Remote.RequestPackage),typeof(Regulus.Remote.ResponsePackage),typeof(Regulus.Remote.ServerToClientOpCode),typeof(System.Boolean),typeof(System.Byte[]),typeof(System.Byte[][]),typeof(System.Char),typeof(System.Char[]),typeof(System.Guid),typeof(System.Int32),typeof(System.Int64),typeof(System.Single),typeof(System.String)).Describers);
                        _MemberMap = new Regulus.Remote.MemberMap(new System.Reflection.MethodInfo[] {new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IAccountService,System.String>>)((ins,_1) => ins.FindAccount(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarm,System.Guid>>)((ins,_1) => ins.Fire(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarm,System.Guid,Minimal.FishHunter.Common.ROTATION>>)((ins,_1,_2) => ins.Rotation(_1,_2))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarm,System.Guid>>)((ins,_1) => ins.Exit(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarm,System.Guid,System.Int32>>)((ins,_1,_2) => ins.Hit(_1,_2))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarm,System.Guid,Minimal.FishHunter.Common.WEAPON>>)((ins,_1,_2) => ins.ChangeWeapon(_1,_2))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IFishFarmService,System.Guid>>)((ins,_1) => ins.Query(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserFishFarmPlayer>>)((ins) => ins.Fire())).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserFishFarmPlayer>>)((ins) => ins.Exit())).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserFishFarmPlayer,Minimal.FishHunter.Common.WEAPON>>)((ins,_1) => ins.ChangeWeapon(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserFishFarmPlayer,System.Int32>>)((ins,_1) => ins.Hit(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserFishFarmPlayer,Minimal.FishHunter.Common.ROTATION>>)((ins,_1) => ins.Rotation(_1))).Method,new Regulus.Utility.Reflection.TypeMethodCatcher((System.Linq.Expressions.Expression<System.Action<Minimal.FishHunter.Common.IUserVerify,System.String,System.String>>)((ins,_1,_2) => ins.Verify(_1,_2))).Method} ,new System.Reflection.EventInfo[]{ typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetEvent("FireEvent") }, new System.Reflection.PropertyInfo[] {typeof(Minimal.FishHunter.Common.IAccount).GetProperty("Id"),typeof(Minimal.FishHunter.Common.IAccount).GetProperty("Password"),typeof(Minimal.FishHunter.Common.IFishFarm).GetProperty("Ticks"),typeof(Minimal.FishHunter.Common.IFishFarm).GetProperty("Players"),typeof(Minimal.FishHunter.Common.IFishFarm).GetProperty("Fishs"),typeof(Minimal.FishHunter.Common.IFishFarmFish).GetProperty("Id"),typeof(Minimal.FishHunter.Common.IFishFarmFish).GetProperty("Type"),typeof(Minimal.FishHunter.Common.IFishFarmFish).GetProperty("Movement"),typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetProperty("Id"),typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetProperty("Location"),typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetProperty("Rotation"),typeof(Minimal.FishHunter.Common.IFishFarmPlayer).GetProperty("Weapon"),typeof(Minimal.FishHunter.Common.IUserFishFarm).GetProperty("Ticks"),typeof(Minimal.FishHunter.Common.IUserFishFarmPlayer).GetProperty("Id") }, new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>[] {new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IAccount),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IAccount>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IAccountService),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IAccountService>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IFishFarm),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IFishFarm>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IFishFarmFish),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IFishFarmFish>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IFishFarmPlayer),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IFishFarmPlayer>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IFishFarmService),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IFishFarmService>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IUserFishFarm),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IUserFishFarm>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IUserFishFarmPlayer),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IUserFishFarmPlayer>()),new System.Tuple<System.Type, System.Func<Regulus.Remote.IProvider>>(typeof(Minimal.FishHunter.Common.IUserVerify),()=>new Regulus.Remote.TProvider<Minimal.FishHunter.Common.IUserVerify>())});
                    }
                    System.Reflection.Assembly Regulus.Remote.IProtocol.Base => _Base;
                    byte[] Regulus.Remote.IProtocol.VerificationCode { get { return new byte[]{91,153,179,122,217,40,77,157,100,142,84,149,63,143,227,74};} }
                    Regulus.Remote.InterfaceProvider Regulus.Remote.IProtocol.GetInterfaceProvider()
                    {
                        return _InterfaceProvider;
                    }

                    Regulus.Remote.EventProvider Regulus.Remote.IProtocol.GetEventProvider()
                    {
                        return _EventProvider;
                    }

                    Regulus.Serialization.ISerializer Regulus.Remote.IProtocol.GetSerialize()
                    {
                        return _Serializer;
                    }

                    Regulus.Remote.MemberMap Regulus.Remote.IProtocol.GetMemberMap()
                    {
                        return _MemberMap;
                    }
                    
                }
            
            