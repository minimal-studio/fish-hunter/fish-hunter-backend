﻿namespace Minimal.FishHunter.Common
{
    public struct Movement
    {
        public Position Start;
        public Position End;
        public Position Middle;
        public float Speed;
        public long Tick;
    }
}
