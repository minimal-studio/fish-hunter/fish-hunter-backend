﻿namespace Minimal.FishHunter.Common
{
    public interface IFishFarmPlayer
    {
        Regulus.Remote.Property<System.Guid> Id { get; }
        Regulus.Remote.Property<int> Location { get; }

        Regulus.Remote.Property<float> Rotation { get; }

        Regulus.Remote.Property<WEAPON> Weapon { get; }

        event System.Action<Bullet> FireEvent;
    }
}
