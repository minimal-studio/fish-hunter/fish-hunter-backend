﻿namespace Minimal.FishHunter.Common
{
    public interface IUserFishFarm
    {
       Regulus.Remote.Property<long> Ticks { get; }
    }
}
