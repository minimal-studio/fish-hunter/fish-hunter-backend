﻿namespace Minimal.FishHunter.Common
{
    public interface IAccountService
    {

        Regulus.Remote.Value<IAccount> FindAccount(string account); 
    }
}
