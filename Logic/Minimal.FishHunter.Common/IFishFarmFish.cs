﻿namespace Minimal.FishHunter.Common
{


    public interface IFishFarmFish
    {
        Regulus.Remote.Property<int> Id { get; }
        Regulus.Remote.Property<int> Type { get; }
        Regulus.Remote.Property<Movement> Movement{ get; }
        
    }
}
