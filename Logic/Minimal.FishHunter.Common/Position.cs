﻿namespace Minimal.FishHunter.Common
{
    public struct Position
    {
        public float x;
        public float y;
    }
}
