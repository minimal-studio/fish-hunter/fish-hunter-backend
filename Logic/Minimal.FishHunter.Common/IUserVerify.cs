﻿namespace Minimal.FishHunter.Common
{
    public interface IUserVerify
    {
        Regulus.Remote.Value<bool> Verify(string account , string password);

    }
}
