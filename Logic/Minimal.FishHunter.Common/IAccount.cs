﻿namespace Minimal.FishHunter.Common
{
    public interface IAccount
    {
        Regulus.Remote.Property<System.Guid> Id { get; }
        Regulus.Remote.Property<string> Password { get; }

    }
}
