﻿namespace Minimal.FishHunter.Common
{
    public interface IUserFishFarmPlayer
    {
        Regulus.Remote.Property<System.Guid> Id { get; }
        void Fire();
        void Exit();
        void ChangeWeapon(WEAPON weapon);
        void Hit(int fish);

        void Rotation(ROTATION rot);
    }
}
