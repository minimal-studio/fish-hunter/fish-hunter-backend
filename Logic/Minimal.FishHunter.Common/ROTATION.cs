﻿namespace Minimal.FishHunter.Common
{
    public enum ROTATION
    {
        STOP,
        RIGHT,
        LEFT
    }
}
