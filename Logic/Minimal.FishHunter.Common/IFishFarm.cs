﻿using Regulus.Remote;
using System;

namespace Minimal.FishHunter.Common
{
    public interface IFishFarm
    {
        Regulus.Remote.Property<long> Ticks { get; }
        Regulus.Remote.INotifier<IFishFarmPlayer> Players { get; }
        Regulus.Remote.INotifier<IFishFarmFish> Fishs { get; }
        void Fire(System.Guid id);
        void Rotation(System.Guid id,ROTATION rotation);
        void Exit(System.Guid id);
        void Hit(System.Guid id , int fish);
        void ChangeWeapon(Guid id, WEAPON weapon);
    }
}
