﻿namespace Minimal.FishHunter.Common
{
    public interface IFishFarmService
    {
        Regulus.Remote.Value<IFishFarm> Query(System.Guid id);
    }
}
